package com.example.haydenclevenger.unimarket;

import android.content.Intent;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.google.firebase.auth.FirebaseAuth;


public class HomeScreen extends AppCompatActivity
    implements FeedFragment.OnFragmentInteractionListener,
        ProfileFragment.OnFragmentInteractionListener {

    // Globals for filtering
    private String searchFilter = "";
    private String categoryFilter = "";

    // ViewPager so we can access Fragment methods
    private MyPagerAdapter pagerAdapter;

    /**
     * Creates toolbar and tabs
     * @param savedInstanceState Saved state from when activity was last closed. Provides optional
     *                           opportunity to restore state.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);

        // Toolbar Setup
        Toolbar homeToolbar = (Toolbar)findViewById(R.id.home_toolbar);
        setSupportActionBar(homeToolbar);

        // Create Tab Layout and format tabs
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.setSelectedTabIndicatorColor(getColor(R.color.colorAccent));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        // Set ViewPager's PageAdapter so it can display items
        ViewPager pager = (ViewPager) findViewById(R.id.view_pager);
        pagerAdapter = new MyPagerAdapter(getSupportFragmentManager(), this);
        pager.setAdapter(pagerAdapter);
        tabLayout.setupWithViewPager(pager);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    /**
     * Method to override the default action bar layout
     * @param menu Custom menu to display on action bar
     * @return True on success, otherwise False
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate custom ActionBar menu
        getMenuInflater().inflate(R.menu.menu_home_screen, menu);

        // Initialize SearchView and add listener
        MenuItem menuItem = menu.findItem(R.id.search);
        SearchView sv = new SearchView(this);
        menuItem.setActionView(sv);
        if(sv != null) {
            sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextChange(String newText) {
                    //searchFilter = newText;
                    return false;
                }

                @Override
                public boolean onQueryTextSubmit(String query) {
                    // Set search filter and repopulate ListView
                    searchFilter = query;
                    // Retrieve PostAdapter from FeedFragment and refresh feed
                    ((FeedFragment) pagerAdapter.getFragment(0)).getPostAdapter().refresh();
                    return false;
                }
            });
        }
        else {
            Log.d("HOME", "SearchView not initialized");
        }

        return true;
    }

    /**
     * Method to sign the current user out and return them to the login screen (Main activity)
     */
    public void logout(MenuItem item) {
        FirebaseAuth.getInstance().signOut();
        startActivity(new Intent(HomeScreen.this, MainActivity.class));
    }

    /**
     * Method to handle Feed Fragment UI
     */
    @Override
    public void onFeedInteraction() {

    }

    /**
     * Method to handle Profile Fragment UI
     */
    @Override
    public void onProfileInteraction() {

    }

    /*
     * Method to handle Add Post Fragment UI
     *
    @Override
    public void OnFragmentInteractionListener() {

    }
    */

    /**
     * Method to navigate back to the Feed fragment
     */
    public void toFeed() {
        ((ViewPager)findViewById(R.id.view_pager)).setCurrentItem(0);
    }

    /**
     * Method to retrieve search bar filter
     */
    public String getSearchFilter() {
        return searchFilter;
    }

    /**
     * Method to retrieve category filter
     */
    public String getCategoryFilter() {
        return categoryFilter;
    }
}