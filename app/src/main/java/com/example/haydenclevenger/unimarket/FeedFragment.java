package com.example.haydenclevenger.unimarket;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FeedFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FeedFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FeedFragment extends ListFragment {

    //firebase references
    private FirebaseDatabase database;
    private StorageReference imageRef;
    private FirebaseUser user;

    //feed variables
    private ArrayList<Post> posts;
    private PostAdapter postAdapter;
    private String schoolFilter;

    private OnFragmentInteractionListener mListener;


    public FeedFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment FeedFragment.
     */
    public static FeedFragment newInstance() {
        FeedFragment fragment = new FeedFragment();
        Bundle args = new Bundle();
        //put values in bundle here
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createFeed();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_feed, container, false);
        //attaching Adapter to ListView
        ListView list = (ListView) rootView.findViewById(android.R.id.list);
        list.setAdapter(postAdapter);
        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
       void onFeedInteraction();
    }

    /**
     * Method to initialize and populate the feed of posts
     */
    private void createFeed() {
        // Initializing feed variables
        posts = new ArrayList<>();
        postAdapter = new PostAdapter(getContext(), posts);

        // Firebase references
        database = FirebaseDatabase.getInstance();
        user = FirebaseAuth.getInstance().getCurrentUser();
        imageRef = FirebaseStorage.getInstance().getReference("images/posts");

        // Retrieve school filter
        getSchoolFilter();

        // Populate feed
        populateFeed();
    }

    /**
     * Method to populate feed
     */
    private void populateFeed() {
        //retrieving posts from firebase
        DatabaseReference postRef = database.getReference("posts");
        postRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                // Filter posts by school, search bar and category
                if(filter(dataSnapshot)) {
                    Log.d("FEED", "Post filtered");
                    return;
                }

                // Get Post from Firebase
                Post post = getPost(dataSnapshot);

                // Add to ArrayList
                posts.add(post);
                postAdapter.notifyDataSetChanged();

                Log.d("FEED", "Post added to feed: posts: " + postAdapter.getCount());
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            }
            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }
            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    /**
     * Method to retrieve post from Firebase
     * Turns DataSnapshot into a Post custom datatype
     * @param snapshot DataSnapshot from Firebase
     * @return snapshot converted into a Post
     */
    private Post getPost(DataSnapshot snapshot) {
        // Get data from DataSnapshot
        HashMap<Object, Object> entry = (HashMap<Object, Object>) snapshot.getValue();

        // Convert to Post
        final Post post = new Post();
        if(entry.get("category") != null) { post.setCategory(entry.get("category").toString()); }
        if(entry.get("title") != null) { post.setTitle(entry.get("title").toString()); }
        if(entry.get("description") != null) { post.setDescription(entry.get("description").toString()); }
        if(entry.get("price") != null) {
            try { post.setCost(Integer.valueOf((String)entry.get("price"))); }
            catch (NumberFormatException e){ post.setCost(99999999); }
        };
        if(entry.get("hasImg") != null) { post.setImg(entry.get("hasImg").equals("true")); }
        if(entry.get("postKey") != null) { post.setPostKey(entry.get("postKey").toString()); }
        if(entry.get("posterID") != null) { post.setPosterID(entry.get("posterID").toString()); }
        if(entry.get("schoolID") != null) { post.setSchoolID(entry.get("schoolID").toString()); }
        if(entry.get("flagged") != null) { post.setFlagged((long) entry.get("flagged")); }
        if(entry.get("time") != null) { post.setTime((long) entry.get("time")); }
        if(entry.get("hasImg") != null) {
            if(entry.get("hasImg").toString().equals("true")) {
                // Image included so download and set post.filePath variable
                StorageReference postImgRef = imageRef.child(post.getPostKey() + ".jpg");
                try {
                    final File localFile = File.createTempFile("img", "jpg");
                    postImgRef.getFile(localFile)
                            .addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                                    post.setFilePath(localFile.getPath());
                                    Log.d("FEED", "Image file: " + post.getFilePath());
                                    postAdapter.notifyDataSetChanged();
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Log.d("FEED", "Downloading image file failed: " + e.getMessage());
                                }
                            });
                }
                catch (java.io.IOException e) {
                    Log.d("FEED", "IOError creating image file: " + e.getMessage());

                }
            }
            else {
                // No image included
                Log.d("FEED", "Image file: " + post.getFilePath());
            }
        }
        return post;
    }

    /**
     * Method for filtering posts from the feed based on school and search bar
     * @param snapshot DataSnapshot from Firebase
     * @return true if post should be filtered out
     */
    private boolean filter(DataSnapshot snapshot) {
        // Get data from DataSnapshot
        HashMap<Object, Object> entry = (HashMap<Object, Object>) snapshot.getValue();

        // Filter by school
        if(entry.get("schoolId") == null || !entry.get("schoolId").toString().equals(schoolFilter)) {
            Log.d("FEED", "Post filtered: " + schoolFilter);
            return true;
        }

        //TODO: Filter by search bar (not case sensitive)
        if(!((HomeScreen)getActivity()).getSearchFilter().isEmpty()) {
            String searchFilter = ((HomeScreen)getActivity()).getSearchFilter().toLowerCase();
            if(entry.get("category") != null
                    && entry.get("category").toString().toLowerCase().contains(searchFilter)) {
                return false;
            }
            else if(entry.get("title") != null
                    && entry.get("title").toString().toLowerCase().contains(searchFilter)) {
                return false;
            }
            else if(entry.get("description") != null
                    && entry.get("description").toString().toLowerCase().contains(searchFilter)) {
                return false;
            }
            else { return true; }
        }

        //TODO: Filter by category
        if(!((HomeScreen)getActivity()).getCategoryFilter().isEmpty()) {
            String categoryFilter = ((HomeScreen)getActivity()).getSearchFilter().toLowerCase();
            if(entry.get("category") != null
                    && entry.get("category").toString().toLowerCase().contains(categoryFilter)) {
                return false;
            }
            else if(entry.get("title") != null
                    && entry.get("title").toString().toLowerCase().contains(categoryFilter)) {
                return false;
            }
            else if(entry.get("description") != null
                    && entry.get("description").toString().toLowerCase().contains(categoryFilter)) {
                return false;
            }
            else { return true; }
        }

        return false;
    }

    /**
     * Method to retrieve school filter specific to user
     */
    private void getSchoolFilter() {
        //getting user's school for filtering
        DatabaseReference userRef = database.getReference("users/" + user.getUid());
        userRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                HashMap<Object, Object> userData = (HashMap<Object, Object>)dataSnapshot.getValue();
                if(userData != null && userData.get("schoolId") != null) {
                    Log.d("FEED", "School filter retrieved");
                    schoolFilter = userData.get("schoolId").toString();
                }
                else {
                    Log.d("FEED", "School filter unavailable");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    /**
     * Method to retrieve the PostAdapter
     * Used by parent activity for filtering
     * @return the PostAdapter in charge of populating the ListView of Posts
     */
    public PostAdapter getPostAdapter() {
        return postAdapter;
    }
}
