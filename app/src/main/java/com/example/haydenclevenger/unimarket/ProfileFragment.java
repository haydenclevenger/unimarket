package com.example.haydenclevenger.unimarket;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ProfileFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfileFragment extends Fragment {

    // Firebase variables
    private ArrayList<Post> posts;
    private PostAdapter postAdapter;
    private FirebaseDatabase database;
    private FirebaseUser user;
    private StorageReference imageRef;

    // Keys for filtering out user's posts
    private ArrayList<String> postKeys;

    // Set if user has no posts to display
    private boolean noPosts = false;

    private OnFragmentInteractionListener mListener;

    public ProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ProfileFragment.
     */
    public static ProfileFragment newInstance() {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        //put params in Bundle here
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createProfile();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        // Initialize and display views
        ListView listView = (ListView) rootView.findViewById(R.id.user_posts_list);
        listView.setAdapter(postAdapter);
        if(noPosts) {
            rootView.findViewById(R.id.no_posts_yet).setVisibility(View.VISIBLE);
        }
        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onProfileInteraction();
    }

    /**
     * Method to initialize and display the user's profile
     */
    private void createProfile() {
        // Initializing profile variables
        posts = new ArrayList<>();
        postAdapter = new PostAdapter(getContext(), posts);
        postKeys = new ArrayList<>();

        // Firebase references
        database = FirebaseDatabase.getInstance();
        user = FirebaseAuth.getInstance().getCurrentUser();
        imageRef = FirebaseStorage.getInstance().getReference("images/posts");

        //TODO: load user's picture, name, and email

        // Retrieve user's post keys
        getPostKeys();

        // Show user's posts
        showUserPosts();
    }

    /**
     * Method to show all user's posts.
     * Uses posts listed under user in Firebase to filter.
     */
    private void showUserPosts() {
        //retrieving posts from Firebase
        if(postKeys.isEmpty()) {
            Log.d("PROFILE", "User has no posts");
            noPosts = true;
            return;
        }
        for (String key : postKeys) {
            DatabaseReference postRef = database.getReference("posts" + "/" + key);
            postRef.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    //get reference to post
                    HashMap<Object, Object> entry = (HashMap<Object, Object>) dataSnapshot.getValue();

                    //convert to Post.class datatype
                    final Post post = new Post();
                    if (entry.get("category") != null) { post.setCategory(entry.get("category").toString()); }
                    if (entry.get("title") != null) {post.setTitle(entry.get("title").toString());}
                    if (entry.get("description") != null) {post.setDescription(entry.get("description").toString());}
                    if (entry.get("price") != null) {post.setCost(Integer.valueOf((String) entry.get("price")));}
                    if (entry.get("hasImg") != null) {post.setImg(entry.get("hasImg").equals("true"));}
                    if (entry.get("postKey") != null) {post.setPostKey(entry.get("postKey").toString());}
                    if (entry.get("posterID") != null) {post.setPosterID(entry.get("posterID").toString());}
                    if (entry.get("schoolID") != null) {post.setSchoolID(entry.get("schoolID").toString());}
                    if (entry.get("flagged") != null) {post.setFlagged((long) entry.get("flagged"));}
                    if (entry.get("time") != null) {post.setTime((long) entry.get("time"));}
                    if (entry.get("hasImg") != null) {
                        if (entry.get("hasImg").toString().equals("true")) {
                            //image included so download and set post.filePath variable
                            StorageReference postImgRef = imageRef.child(post.getPostKey() + ".jpg");
                            try {
                                final File localFile = File.createTempFile("img", "jpg");
                                postImgRef.getFile(localFile)
                                        .addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                                            @Override
                                            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                                                post.setFilePath(localFile.getPath());
                                                Log.d("PROFILE", "Image file: " + post.getFilePath());
                                                postAdapter.notifyDataSetChanged();
                                            }
                                        })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                Log.d("PROFILE", "Downloading image file failed: " + e.getMessage());
                                            }
                                        });
                            } catch (java.io.IOException e) {
                                Log.d("PROFILE", "IOError creating image file: " + e.getMessage());

                            }
                        } else {
                            //no image included
                            Log.d("PROFILE", "Image file: " + post.getFilePath());
                        }

                        //add to ArrayList
                        posts.add(post);
                        postAdapter.notifyDataSetChanged();

                        Log.d("FEED", "Post added: posts: " + postAdapter.getCount());
                    }
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                }
                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                }
                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                }
                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });
        }
    }

    /**
     * Method to retrieve keys of posts belonging to the user.
     * Updates the ArrayList of Strings postKeys.
     */
    private void getPostKeys() {
        //getting user's post keys for filtering
        final DatabaseReference userPosts = database.getReference("users/" + user.getUid() + "/Posts");
        userPosts.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                HashMap<Object, Object> userData = (HashMap<Object, Object>)dataSnapshot.getValue();
                if(userData != null) {
                    int i = 0;
                    for(Object key : userData.keySet()) {
                        postKeys.add(key.toString());
                        i++;
                    }
                    Log.d("PROFILE", "User post list retrieved (" + i + " posts)");
                }
                else {
                    Log.d("PROFILE", "User post list unavailable");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
