package com.example.haydenclevenger.unimarket;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Date;
import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AddPostFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AddPostFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddPostFragment extends Fragment {

    // Firebase variables
    private FirebaseDatabase database;
    private FirebaseUser user;

    // Holds users's school ID
    private String schoolID = "";

    private Context context;

    private OnFragmentInteractionListener mListener;

    public AddPostFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment AddPostFragment.
     */
    public static AddPostFragment newInstance() {
        AddPostFragment fragment = new AddPostFragment();
        // Create Bundle and add params for more complicated Fragment creation
        // i.e. Save state when user enters text and switches tabs
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Initialize Firebase variables
        database = FirebaseDatabase.getInstance();
        user = FirebaseAuth.getInstance().getCurrentUser();

        // School ID from Firebase
        DatabaseReference myRef = database.getReference("users/" + user.getUid());
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                HashMap<Object, Object> userData = (HashMap<Object, Object>)dataSnapshot.getValue();
                if(userData != null && userData.get("schoolId") != null) {schoolID = userData.get("schoolId").toString();}
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_add_post, container, false);
        context = getActivity().getApplicationContext();
        if(context == null) {
            Log.d("ADD", "Context is null");
        }
        else { Log.d("ADD", "Context not null"); }

        // Set click listener for submission button
        final TextView b = (TextView) rootView.findViewById(R.id.post);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadPost(b);
            }
        });

        return rootView;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onAddPostInteraction();
    }

    /**
     * Method to add a post to Firebase
     * @param view The button that activated the method
     */
    public void uploadPost(View view) {
        // Information from posting template
        String title = ((TextView)getView().findViewById(R.id.title)).getText().toString();
        String price = ((TextView)getView().findViewById(R.id.price)).getText().toString();
        String description = ((TextView)getView().findViewById(R.id.description)).getText().toString();
        String category = ((Spinner) getView().findViewById(R.id.category)).getSelectedItem().toString();

        // Make sure all fields filled out
        if(title.isEmpty() || price.isEmpty() || description.isEmpty() || category.equals("Category")) {
            Toast.makeText(context, "All fields required", Toast.LENGTH_SHORT).show();
            return;
        }

        // Firebase database reference
        DatabaseReference posts = database.getReference("posts");
        DatabaseReference newPost = posts.push();

        // Create Map
        HashMap<Object, Object> map = new HashMap<>();
        map.put("title", title);
        map.put("price", price);
        map.put("description", description);
        map.put("category", category);
        map.put("flagged", 0);
        map.put("posterID", user.getUid());
        map.put("schoolId", schoolID);
        map.put("time", (new Date()).getTime());
        map.put("hasImg", false);                   //TODO: add images
        map.put("postKey", newPost.getKey());

        // Add post to Firebase
        newPost.setValue(map);
        DatabaseReference userPost = database.getReference("users/" + user.getUid()
                + "/Posts/" + newPost.getKey());
        userPost.setValue(true);

        // Notify user
        Toast.makeText(context, "Post successfully submitted", Toast.LENGTH_SHORT).show();

        //Clear user entered fields
        ((TextView) getView().findViewById(R.id.title)).setText("");
        ((TextView) getView().findViewById(R.id.price)).setText("");
        ((TextView) getView().findViewById(R.id.description)).setText("");

        //navigate back to feed tab
        ((HomeScreen)getActivity()).toFeed();

        //TODO: fix out of memory error?
    }
}
