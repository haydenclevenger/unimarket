package com.example.haydenclevenger.unimarket;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Custom Array Adapter to fill out entries of ListView with posts
 * Hayden Clevenger - haydenclevenger@sandiego.edu
 * April 2, 2017
 */

public class PostAdapter extends ArrayAdapter<Post> {

    private LayoutInflater inflater;
    private ArrayList<Post> posts;


    /**
     * Contructor
     * @param context The context this object is being created in
     * @param posts An ArrayList of Posts for this PostAdapter to use
     */
    public PostAdapter(Context context, ArrayList<Post> posts) {
        super(context, R.layout.post_layout, posts);
        this.inflater = LayoutInflater.from(context);
        this.posts = posts;
    }

    // TODO: shrink this method down
    /**
     * Method to convert a Post to a View for display in a ListView
     * @param position Indexes into posts to return specific Post
     * @param convertView Old View to reuse if possible
     * @param parent The parent that this view will eventually be attached to
     * @return A View corresponding to the Post at the specified position
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //get data item for this position
        Post post = getItem(position);
        //check if existing view is being reused, otherwise inflate the view
        if(convertView == null) {
            convertView = inflater.inflate(R.layout.post_layout, parent, false);
        }
        //Populate views with data
        TextView title = (TextView) convertView.findViewById(R.id.title);
        title.setText(post.getTitle());

        TextView subTitle = (TextView) convertView.findViewById(R.id.subTitle);
        subTitle.setText(post.getDescription());

        TextView cost = (TextView) convertView.findViewById(R.id.cost);
        cost.setText(getContext().getResources().getString(R.string.cost, post.getCost()));

        ImageView image = (ImageView) convertView.findViewById(R.id.image);
        if(post.getFilePath().compareTo("no file") == 0) {
            //no file so set to generic img drawable
            Resources resources = getContext().getResources();
            Drawable d = resources.getDrawable(android.R.drawable.ic_menu_report_image);
            image.setImageDrawable(d);
        }
        else {
            //file uploaded so create drawable
            //look at file tags to see camera orientation when taken

            try {
                ExifInterface ei = new ExifInterface(post.getFilePath());
                Bitmap bm;
                switch (ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1)) {
                    case 1: Log.d("FEED", "camera upright");
                        bm = BitmapFactory.decodeFile(post.getFilePath());
                        image.setImageBitmap(bm);
                        break;
                    case 3: Log.d("FEED", "camera upside down");
                        break;
                    case 6: Log.d("FEED", "camera tilted left");
                        Matrix m = new Matrix();
                        m.postRotate(90);
                        bm = BitmapFactory.decodeFile(post.getFilePath());
                        bm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(),bm.getHeight(), m, true);
                        image.setImageBitmap(bm);
                        break;
                    case 8: Log.d("FEED", "camera tilted right");
                        break;
                    default: Log.d("FEED", "invalid camera tilt tags");
                        break;
                    //TODO: possibly force portrait orientation?
                }
            }
            catch (IOException e){
                Log.d("FEED", "IOerror creating ExifInterface: " + e.getMessage());
                image.setImageDrawable(Drawable.createFromPath(post.getFilePath()));
            }

//            image.setImageDrawable(Drawable.createFromPath(post.getFilePath()));
        }

        //return completed view to render on screen
        return convertView;
    }

    @Override
    public int getCount() {
        return posts.size();
    }

    /**
     * Method to empty the feed of posts and regenerate it
     * Used for filtering
     */
    public void refresh() {
        posts.clear();
        this.notifyDataSetChanged();
    }
}
