# README #

### What is this repository for? ###

* UniMarket is an online marketplace that provides students with a safe means to buy and sell goods and services
* The project is still under development

### How do I get set up? ###

* This git repository is public!
* To run the app I recommend using the most recent version of Android Studio's emulators with minimum API >= 23
* Registration requires registration and verification with a .edu email

### Contribution guidelines ###

* Contributions are welcome and will be reviewed on a case by case basis
* Areas under development will sometimes be noted by a TODO comment but new areas are always open to improvement

### Who do I talk to? ###

* Hayden Clevenger - hayden.clev@gmail.com