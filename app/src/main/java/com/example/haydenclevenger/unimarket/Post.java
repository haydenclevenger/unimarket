package com.example.haydenclevenger.unimarket;


/**
 * Class that defines a post to populate a listView
 * Hayden Clevenger - haydenclevenger@sandiego.edu
 * April 2, 2017
 */

public class Post {
    //fields
    private String category;
    private String title;
    private String description;
    private int cost;
    private boolean hasImg;     // Indicates there is an image associated with this post
    private String postKey;     // Unique key for post
    private String posterID;    // Unique user ID for owner of this post
    private String schoolID;    // School ID of post owner
    private long flagged;       // Flag count for users who have reported this post
    private long time;          // Time since epoch that post was created
    private String filePath;    // Reference to keep track of photo

    //constructors
    public Post() {
        category = "category";
        title = "title";
        description = "description";
        cost = 0;
        hasImg = false;
        postKey = "key";
        posterID = "user";
        schoolID = "school";
        flagged = 0;
        time = 0;
        filePath = "no file";
    }
    public Post(String key, String user, String school) {
        category = "category";
        title = "title";
        description = "description";
        cost = 0;
        hasImg = false;
        postKey = key;
        posterID = user;
        schoolID = school;
        flagged = 0;
        time = 0;
        filePath = "no file";
    }

    public Post(String cat,
                String tit,
                String descrip,
                int c,
                boolean img,
                String key,
                String user,
                String school,
                long flag,
                long t) {
        category = cat;
        title = tit;
        description = descrip;
        cost = c;
        hasImg = img;
        postKey = key;
        posterID = user;
        schoolID = school;
        flagged = flag;
        time = t;
        filePath = "no file";
    }

    //methods
    public String getCategory() {
        return this.category;
    }

    public void setCategory(String cat) { this.category = cat; }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String tit) {
        this.title = tit;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String descrip) {
        this.description = descrip;
    }

    public int getCost() { return this.cost; }

    public void setCost(int p) { this.cost = p; }

    public boolean getImg() {
        return this.hasImg;
    }

    public void setImg(boolean img) {
        this.hasImg = img;
    }

    public String getPostKey() {
        return this.postKey;
    }

    public void setPostKey(String key) {
        this.postKey = key;
    }

    public String getPosterID() {
        return this.posterID;
    }

    public void setPosterID(String user) {
        this.posterID = user;
    }

    public String getSchoolID() {
        return this.schoolID;
    }

    public void setSchoolID(String school) {
        this.schoolID = school;
    }

    public long getFlagged() { return this.flagged; }

    public void setFlagged(long flag) {
        this.flagged = flag;
    }

    public void flag() {
        this.flagged++;
    }

    public long getTime() {
        return this.time;
    }

    public void setTime(long t) {
        this.time = t;
    }

    public String getFilePath() { return this.filePath; }

    public void setFilePath(String f) { this.filePath = f; }
}
