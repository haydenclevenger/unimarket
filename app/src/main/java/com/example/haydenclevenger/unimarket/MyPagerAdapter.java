package com.example.haydenclevenger.unimarket;

import android.content.Context;
import android.graphics.drawable.Drawable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.core.content.ContextCompat;
import androidx.collection.SparseArrayCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import android.view.ViewGroup;


/**
 * This class defines a custom MyPagerAdapter to support tabs on the app HomeScreen
 * Created by Hayden Clevenger on 5/6/17.
 */

public class MyPagerAdapter extends FragmentPagerAdapter {

    final private int PAGE_COUNT = 3;

    // Array of Images used for Tab icons
    private int[] imageResId = {
            R.drawable.ic_list_black_24dp,
            R.drawable.ic_account_circle_black_24dp,
            R.drawable.ic_add_circle_outline_black_24dp
    };

    private Context context;

    // Mapping of fragments so their methods can be accessed by the parent activity
    SparseArrayCompat<Fragment> fragmentArray = new SparseArrayCompat<>(PAGE_COUNT);

    public MyPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    /**
     * Method to instantiate a new Fragment
     * Overridden to keep track of fragments for access later on
     * @param container The containing View in which the page will be shown
     * @param position The position of the page to be instantiated
     * @return The Fragment representing the new page
     */
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        fragmentArray.put(position, fragment); // Add Fragment to custom mapping
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        fragmentArray.remove(position); // Remove Fragment from custom mapping
        super.destroyItem(container, position, object);
    }


    /**
     * Method to return new Fragments of specific type based on tab position
     * Used to populate the tabs in a specific order for our layout
     * @param position Position of the tab being filled
     * @return a Fragment to fill the tab at the specified position
     */
    @Override
    public Fragment getItem(int position) {
        switch(position) {
            case 0:
                return new FeedFragment();
            case 1:
                return new ProfileFragment();
            case 2:
                return new AddPostFragment();
            default:
                return null;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        Drawable image = ContextCompat.getDrawable(context, imageResId[position]);
        image.setBounds(0, 0, image.getIntrinsicWidth(), image.getIntrinsicHeight());
        SpannableString sb = new SpannableString(" ");
        ImageSpan imageSpan = new ImageSpan(image, ImageSpan.ALIGN_BOTTOM);
        sb.setSpan(imageSpan, 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return sb;
    }


    @Override
    public int getCount() { return PAGE_COUNT; }

    /**
     * Method to retrieve Fragment from mapping
     * @param position Specifies which Fragment we want
     * @return an Object representing our fragment
     */
    public Object getFragment(int position) {
        return fragmentArray.get(position);
    }
}
