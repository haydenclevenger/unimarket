package com.example.haydenclevenger.unimarket;

import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    //user authentication variables
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    //Firebase database reference
    private FirebaseDatabase database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //user authentication objects
        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    //user is signed in
                    Log.d("USER", "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    //user is signed out
                    Log.d("USER", "onAuthStateChanged:signed_out");
                }
            }
        };
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if(mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    /**
     * Method to determine whether user wants to sign up or login
     * Calls either signUp or createAccount
     * @param view View that called this method
     */
    public void registerOrLogin(View view) {
        TextView emailView = (TextView)findViewById(R.id.email);
        String email = emailView.getText().toString();
        TextView passwordView = (TextView)findViewById(R.id.password);
        String password = passwordView.getText().toString();
        TextView verifyView = (TextView)findViewById(R.id.verify);
        if(verifyView.getVisibility() == View.VISIBLE) {
            String verify = verifyView.getText().toString();
            if(validInput(email, password, verify)) {
                createAccount(email, password);
            }
        }
        else {
            if(validInput(email, password, null)) {
                signIn(email, password);
            }
        }
    }

    /**
     * Method to create a new user account with Firebase
     * @param email User entered email
     * @param password User entered password
     */
    private void createAccount(String email, String password) {
        Log.d("AUTH", "createAccount:" + email);

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d("AUTH", "createUserWithEmail:onComplete:" + task.isSuccessful());
                        if(!task.isSuccessful()) {
                            Toast.makeText(MainActivity.this, "User registration failed", Toast.LENGTH_LONG).show();
                        }
                        else {
                            Toast.makeText(MainActivity.this, "User registration successful", Toast.LENGTH_LONG).show();
                            sendEmailVerification();
                            addUser();
                        }
                    }
                });
    }

    /**
     * Method to sign the user in.
     * Relies on Firebase user authentication backend.
     * @param email User entered email address
     * @param password User entered password
     */
    private void signIn(String email, String password) {
        Log.d("USER", "signIn:" + email);
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d("AUTH", "signInWithEmail:onComplete" + task.isSuccessful());
                        if(!task.isSuccessful()) {
                            Toast.makeText(MainActivity.this, "User login failed", Toast.LENGTH_LONG).show();
                        }
                        else {
                            Toast.makeText(MainActivity.this, "User login successful", Toast.LENGTH_LONG).show();
                            startActivity(new Intent(MainActivity.this, HomeScreen.class));
                        }
                    }
                });
    }

    /**
     * Method to send an email verification to a new user.
     */
    private void sendEmailVerification() {
        final FirebaseUser user = mAuth.getCurrentUser();
        user.sendEmailVerification()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.isSuccessful()) {
                            Toast.makeText(MainActivity.this, "Email verification sent", Toast.LENGTH_LONG).show();
                        }
                        else {
                            Toast.makeText(MainActivity.this, "Email verification failed", Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    /**
     * Method to change the UI between signup/login.
     * @param view Button that called the method
     */
    public void changeUI(View view) {
        TextView verify = (TextView)findViewById(R.id.verify);
        Button login = (Button)findViewById(R.id.go);
        Button changeUI = (Button)findViewById(R.id.changeUI);
        if(verify.getVisibility() == View.VISIBLE) {
            //change to login screen
            verify.setVisibility(View.INVISIBLE);
            login.setText(R.string.login);
            changeUI.setText(R.string.no_account);
        }
        else {
            //change to signup screen
            verify.setVisibility(View.VISIBLE);
            login.setText(R.string.sign_up);
            changeUI.setText(R.string.yes_account);
        }
    }

    /**
     * Method to check formatting of user registration information.
     * Checks if passwords match and if email ends in .edu.
     * @param email User entered email address.
     * @param password User entered password.
     * @param verify User entered password verification.
     * @return True if passwords match and if email ends in .edu.
     */
    private boolean validInput(String email, String password, String verify) {
        if(verify != null && !password.equals(verify)) {
            Toast.makeText(MainActivity.this, "Passwords do not match", Toast.LENGTH_LONG).show();
            return false;
        }
        else if(!email.endsWith(".edu")) {
            Toast.makeText(MainActivity.this, "Email must end in .edu", Toast.LENGTH_LONG).show();
            return false;
        }
        else
            return true;
    }

    /**
     * Method to add user to Firebase databse.
     */
    private void addUser() {
        //reference to user
        FirebaseUser user = mAuth.getCurrentUser();
        //reference to "users" section of database
        DatabaseReference myRef = database.getReference("users");
        if(user != null) {
            //create reference to new child under users section
            DatabaseReference child = myRef.child(user.getUid());
            //get email
            String email = user.getEmail();
            //create hashmap of user values
            HashMap<Object, Object> map = new HashMap<>();
            map.put("email", email);
            map.put("schoolId", email.substring(email.lastIndexOf('@')+1));
            //set user value to hashmap
            child.setValue(map);
        }
    }
}